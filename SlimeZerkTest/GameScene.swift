//
//  GameScene.swift
//  SlimeZerkTest
//
//  Created by Parrot on 2019-02-25.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    // MARK: Variables for tracking time
    private var lastUpdateTime : TimeInterval = 0
    var orange:Orange!

    // MARK: Sprite variables
    var player:SKSpriteNode = SKSpriteNode()
    var upButton:SKSpriteNode = SKSpriteNode()
    var downButton:SKSpriteNode = SKSpriteNode()
    var rightButton:SKSpriteNode = SKSpriteNode()
    var leftButton:SKSpriteNode = SKSpriteNode()
    var wall:SKSpriteNode = SKSpriteNode()
    var exit:SKSpriteNode = SKSpriteNode()
    var enemy:SKSpriteNode = SKSpriteNode()
    var music:SKSpriteNode = SKSpriteNode()
    
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    var lives = 3
    var levelNumber = 1
    var musicState = 0
    var xn:CGFloat = 0
    var yn:CGFloat = 0
     let ENEMY_SPEED:CGFloat = 5
    
    // MARK: Scoring and Lives variables
    
    
    // MARK: Game state variables
    
    
    
    // MARK: Default GameScene functions
    // -------------------------------------
    override func didMove(to view: SKView) {
        self.lastUpdateTime = 0
        
        // get sprites from Scene Editor
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        self.upButton = self.childNode(withName: "upButton") as! SKSpriteNode
        self.downButton = self.childNode(withName: "downButton") as! SKSpriteNode
        self.rightButton = self.childNode(withName: "rightButton") as! SKSpriteNode
        self.leftButton = self.childNode(withName: "leftButton") as! SKSpriteNode
        self.wall = self.childNode(withName: "wall") as! SKSpriteNode
        self.exit = self.childNode(withName: "exit") as! SKSpriteNode
        self.enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        self.music = self.childNode(withName: "musicButton") as! SKSpriteNode
        
//        self.shapeNode.lineWidth = 20
//        self.shapeNode.lineCap = .round
//        self.shapeNode.strokeColor = UIColor(red: 0.5294, green: 0, blue: 0.5765, alpha: 0.8)
//        addChild(shapeNode)
        
        
        // get labels from Scene Editor
        self.livesLabel = self.childNode(withName: "livesLabel") as! SKLabelNode
       // self.livesLabel.text = "liveRemaining:\(self.lives)"
        // player = 1 and wall = 16 and enemy 2 orange = 4
        
        
        self.player.physicsBody?.isDynamic = true
       self.enemy.physicsBody?.isDynamic = true
       self.wall.physicsBody?.collisionBitMask = 7
      self.player.physicsBody?.collisionBitMask = 18
      self.enemy.physicsBody?.collisionBitMask = 20
      self.Enemymove()
        
    
        
        }
    
    
    func moveUp()
    {
        let upAction = SKAction.moveTo(y: frame.size.height, duration: 5.0)
        self.player.run(upAction)
    }
    func moveDown() {
        let downAction = SKAction.moveTo(y: -frame.size.height, duration: 5.0)
        self.player.run(downAction)
        
    }
    func moveRight() {
        self.player.xScale = 1
        let rightAction = SKAction.moveTo(x:frame.size.width, duration: 5.0)
        self.player.run(rightAction)
        
    }
    func moveLeft() {
        self.player.xScale = -1
        let leftAction = SKAction.moveTo(x:-frame.size.width, duration: 5.0)
        self.player.run(leftAction)
        }
 
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        let mousePosition = touch?.location(in: self)
        
        let spriteTouched = self.atPoint(mousePosition!)
        
        if (spriteTouched.name == "upButton") {
           
            self.moveUp()
            
        }
        else if(spriteTouched.name == "downButton")
        {
            self.moveDown()
        }
        else if(spriteTouched.name == "rightButton")
        {
            self.moveRight()
        }
        else if(spriteTouched.name == "leftButton")
        {
            self.moveLeft()
        }
            
        else if(spriteTouched.name == "bButton")
        { // adding orange and shoothing them straight.
            // Orange collison only with enemy and wall
            self.orange = Orange()
            self.orange.position = self.player.position
            self.orange.name = "orange"
            addChild(self.orange)
            self.orange.physicsBody?.categoryBitMask = 4
            self.orange.physicsBody?.isDynamic = true
            self.orange.physicsBody?.collisionBitMask = 18
            let moveOrange = SKAction.moveTo(x: size.width, duration: 2.0)
            self.orange.run(moveOrange)
            orange.physicsBody = SKPhysicsBody(rectangleOf: self.orange.frame.size)
            
    }
        else if(spriteTouched.name == "musicButton")
        {
            // play backgroundMusic
             let music = SKAction.playSoundFileNamed("BackgroundMusic/SpaceTrip.mp3", waitForCompletion: false)
           if(musicState==0)
           {
            self.run(music)
            musicState = 1
            }
            if(musicState==1)
            {
                self.run(SKAction.pause())
               
            }
            
            
        }
        }
    

    
    // make EnemyMove in the game scene
    func Enemymove() {
        // Make Enemy move
        let x1 = self.size.width / 2 * -1
        let y1 = self.size.height / 2
        let x2 = x1
        let y2 = y1 * -1
        let action1 = SKAction.moveBy(x: x1, y: y1, duration: 2)
        let action2 = SKAction.moveBy(x: x2, y: y2, duration: 2)
        let action3 = action2.reversed()
        let action4 = action1.reversed()
        let sequence = SKAction.sequence([action1,action2,action3,action4])
        self.enemy.run(SKAction.repeatForever(sequence))
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        if (self.enemy.position.x <= 0 ||
            self.enemy.position.x >= self.size.width) {
            self.xn = self.xn * -1
        }
        
        // 2. check top and bottom of screen
        if (self.enemy.position.y <= 0 ||
            self.enemy.position.y >= self.size.height) {
            self.yn = self.yn * -1
        }
        // Make enemyMove
        self.enemy.position.x = self.enemy.position.x + self.xn*ENEMY_SPEED
        self.enemy.position.y = self.enemy.position.y + self.yn*ENEMY_SPEED
        
        
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }

        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
    
        // HINT: This code prints "Hello world" every 5 seconds
        if (dt > 20) {
            self.lastUpdateTime = currentTime
        }
        
        // player and enemy intersection
        if(self.player.intersects(self.enemy))
        {
            self.gameOver()
            perform(#selector(GameScene.gameOver), with: nil,
                    afterDelay: 3)
        }
        // player and exit wall intersction leads to new level
        if(self.player.intersects(self.exit))
        {
            self.levelNumber =  self.levelNumber + 1
            let scene = GameScene(fileNamed:"BerzerkLevel\(self.levelNumber).sks")
            scene!.scaleMode = scaleMode
            view?.presentScene(scene)
            print("exit")
           
        
        }
        // in starting orange is null so checking null condition
        if (orange == nil)
        {
            print("orange is null")
        }
      
            // when orange is added and checking wether the orange hitting enemy
            // if yes move to next level
            else if (self.orange.intersects(self.enemy))
        {  self.levelNumber =  self.levelNumber + 1
            let scene = GameScene(fileNamed:"BerzerkLevel\(self.levelNumber).sks")
            scene!.scaleMode = scaleMode
            view?.presentScene(scene)
            print("exit")
        }
    
        if(self.lives == 0 )
        {
        
            self.gameOver()
        }
        
    }
    
    
    @objc func gameOver() {
        self.livesLabel.text = "GameOver"
        self.livesLabel.position = CGPoint(x: size.width/2, y: size.height/2)
        Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (Timer) in
            self.lives = 3
            self.levelNumber = 1
            let scene = GameScene(fileNamed:"BerzerkLevel\(self.levelNumber)")
            scene!.scaleMode = self.scaleMode
            self.view?.presentScene(scene)
            print("exit")
            
            
        }
        
     }

    
}
